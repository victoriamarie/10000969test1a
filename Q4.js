/*
Name:   Victoria Laubli 10000969
Date:   29 March 2018
Purpose: To check when the user inputs a letter of the alphabet ("a to z" or "A to Z")
*/


//Declare variables
let value                  
let type                


//Prompt for and get input from user
value = prompt("Enter a letter of the alphabet to determine if is is a vowel or consonant:")


//Determine type of rounding (up or down)
switch(value) {
   case "a":                             //Ends with a to u
   case "e":
   case "i":
   case "o":
   case "u":
   case "A":
   case "E":
   case "I":
   case "O":
   case "U":
    type = "vowel"                      //Apply consonant
       break
   default:                            //Unknown input, consonant
       type = "consonant"
}


//Display heading
console.log("This application determines if your input is a vowel or a consonant")
console.log("-------------------------------------------------------------------")
console.log(${type} is a $(value))